package jpp.digraph.exceptions;

public class DiGraphException extends Exception{

	public DiGraphException(){
		super();
	}
	public DiGraphException(String message){
		super(message);
	}
}

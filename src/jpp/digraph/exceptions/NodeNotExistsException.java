package jpp.digraph.exceptions;

import jpp.digraph.graph.INode;

public class NodeNotExistsException extends DiGraphException{
	private INode node;
	public NodeNotExistsException(INode node){
		super();
		this.node = node;
	}
	public NodeNotExistsException(String msg, INode node){
		super(msg);
		this.node = node;
	}
	public INode getNode(){
		return this.node;
	}
}







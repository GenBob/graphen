/**
 * 
 */
package jpp.digraph.graph;


/**
 * @author genbob
 *
 */
public class CostEdge<N extends INode> implements ICostEdge<N>{
	
	private final String id;
	private N source;
	private N target;
	private double cost;
	private String description;
	
	public CostEdge(String id, N source, N target, double cost) throws IllegalArgumentException{
		if(id == null || id.isEmpty()) throw new IllegalArgumentException("Invalid id!");
		this.id = id;
		this.source = source;
		this.target = target;
		this.cost = cost;
		this.description = "";
	}
	public CostEdge(String id, N source, N target, double cost, String description) throws IllegalArgumentException{
		if(id == null || id.isEmpty()) throw new IllegalArgumentException("Invalid id");
		this.id = id;
		this.source = source;
		this.target = target;
		this.cost = cost;
		this.description = description;
		
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public N getSource() {
		return this.source;
	}

	@Override
	public N getTarget() {
		return this.target;
	}

	@Override
	public double getCost() {
		return this.cost;
	}
	
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("Edge id: " + this.id + "\n");
		sb.append("Source: " + this.source.getId() + " Target: " + this.target.getId()+ "\n");
		sb.append("Cost: " + this.cost + " Discription: " + this.description + "\n");
		return sb.toString();
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CostEdge other = (CostEdge) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
	
	
	
	
	
	

}

/**
 * 
 */
package jpp.digraph.graph;

import java.util.ArrayList;

/**
 * @author genbob
 *
 */
public class XYNode implements IXYNode{
	private final String id;
	private int x;
	private int y;
	private String description;
	private INode preccesor;
	private State state;
	public double cost;
	public XYNode(String id, int x, int y)throws IllegalArgumentException{
		this.state = State.UNKNOWN;
		if(id == null || id.isEmpty()) throw new IllegalArgumentException("Invalid id!");
		this.description = "";
		this.id = id;
		this.x = x;
		this.y = y;
	}
	public XYNode(String id, int x, int y, String description){
		this.state = State.UNKNOWN;
		if(id == null || id.isEmpty()) throw new IllegalArgumentException("Invalid id!");
		this.id = id;
		this.x = x;
		this.y = y;
		this.description = description;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public int getX() {
		return this.x;
	}
	
	@Override
	public int getY() {
		return this.y;
	}

	
	public void setState(State state){
		this.state = state;
	}
	
	public State getState(){
		return this.state;
	}
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append("Node id: " + this.id + "\n");
		sb.append("X: "+this.x + " Y: " + this.y + " Discription: " + this.description + "\n");
		return sb.toString();
	}
	
	
	
	
	public INode getPre(){
		return this.preccesor;
	}
	
	public void setPre(INode node){
		this.preccesor = node;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XYNode other = (XYNode) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		
		return true;
	}
	
	

	
	
	
	
	
	

}

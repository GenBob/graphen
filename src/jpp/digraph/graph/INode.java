package jpp.digraph.graph;

import java.util.ArrayList;

/**
 * Interface fuer einen Knoten.
 */
public interface INode {

    /**
     * gibt die ID eines Knoten zurueck.
     * 
     * @return ID
     */
    public String getId();
 
    /**
     * gibt die Beschreibung eines Knoten zurueck.
     * 
     * @return Beschreibung
     */
    public String getDescription();
    
    
    enum State{UNKNOWN,IN_WORK,READY};
    public boolean equals(Object obj);
    public int hashCode();
   
}
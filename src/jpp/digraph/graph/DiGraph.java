/**
 * 
 */
package jpp.digraph.graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.PriorityQueue;

import jpp.digraph.exceptions.EdgeNotExistsException;
import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.exceptions.NodeNotExistsException;

/**
 * @author genbob
 *
 */
public class DiGraph<N extends INode, E extends IEdge<N>> implements IDiGraph<N, E> {
	private Collection<N> nodes;
	private Collection<E> edges;
	private String id;

	public DiGraph() {
		this.nodes = new ArrayList<N>();
		this.edges = new ArrayList<E>();
	}

	public DiGraph(Collection<N> nodes) {
		this.nodes = new ArrayList<N>();
		for (N node : nodes) {
			if (!this.nodes.contains(node))
				this.nodes.add(node);
		}
		this.edges = new ArrayList<E>();

	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	/**
	 * 
	 * @param nodes
	 * @param edges
	 * @throws InvalidEdgeException
	 *             thrown if collection contains an edge which connects one or
	 *             two nodes which are not holden by the nodes collection
	 */
	public DiGraph(Collection<N> nodes, Collection<E> edges) throws InvalidEdgeException {
		for (E edge : edges) {
			if (!nodes.contains(edge.getSource()) || !nodes.contains(edge.getTarget()))
				throw new InvalidEdgeException("Invalide edge!", edge);
		}
		this.nodes = new ArrayList<N>();
		this.edges = new ArrayList<E>();
		for (N node : nodes) {
			if (!this.nodes.contains(node))
				this.nodes.add(node);
		}
		for (E edge : edges) {
			if (!this.edges.contains(edge))
				this.edges.add(edge);
		}
	}

	@Override
	public int getNodeCount() {
		return this.nodes.size();
	}

	@Override
	public int getEdgeCount() {
		return this.edges.size();
	}

	@Override
	public Collection<N> getNodes() {
		return this.nodes;
	}

	@Override
	public Collection<E> getEdges() {
		return this.edges;
	}

	@Override
	public void setNodes(Collection<N> nodes) {
		this.nodes.clear();
		this.edges.clear();
		for (N node : nodes) {
			if (!this.nodes.contains(node))
				this.nodes.add(node);
		}

	}

	@Override
	public void setEdges(Collection<E> edges) throws InvalidEdgeException {
		if (edges == null)
			return;

		for (E edge : edges) {
			if (!this.nodes.contains(edge.getSource()) || !this.nodes.contains(edge.getTarget()))
				throw new InvalidEdgeException("Invalide edge!", edge);
		}
		this.edges.clear();
		for (E edge : edges) {
			if (!this.edges.contains(edge))
				this.edges.add(edge);
		}
	}

	public N getNodeById(String id) {
		for (N node : this.nodes) {
			if (node.getId().equalsIgnoreCase(id)) {
				return node;
			}
		}
		return null;
	}

	@Override
	public void setGraph(Collection<N> nodes, Collection<E> edges) throws InvalidEdgeException {
		for (E edge : edges) {
			if (!nodes.contains(edge.getSource()) || !nodes.contains(edge.getTarget()))
				throw new InvalidEdgeException("Invalide edge!", edge);
		}
		this.nodes.clear();
		this.nodes.addAll(nodes);
		this.edges.clear();
		this.edges.addAll(edges);
	}

	@Override
	public void removeNodes() {
		this.edges.clear();
		this.nodes.clear();
	}

	@Override
	public void removeEdges() {
		this.edges.clear();
	}

	@Override
	public boolean containsNode(N node) {
		return this.nodes.contains(node);
	}

	@Override
	public boolean containsEdge(E edge) {
		return this.edges.contains(edge);
	}

	@Override
	public boolean containsEdge(N source, N target) {
		for (E edge : this.edges) {
			if (edge.getTarget().equals(target) && edge.getSource().equals(source)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void addNode(N node) {
		if (node == null)
			return;
		if (!this.nodes.contains(node))
			this.nodes.add(node);
	}

	@Override
	public void addEdge(E edge) throws InvalidEdgeException {
		if (edge == null)
			return;
		if (!this.nodes.contains(edge.getSource()) || !this.nodes.contains(edge.getTarget())) {
			throw new InvalidEdgeException("Invalid Edge!", edge);
		}
		if (!this.edges.contains(edge)) {
			this.edges.add(edge);
		}
	}

	@Override
	public void removeNode(N node) throws NodeNotExistsException {
		if (!this.nodes.contains(node))
			throw new NodeNotExistsException(node);
		this.nodes.remove(node);
		Iterator<E> iterator = this.edges.iterator();
		while (iterator.hasNext()) {
			E edge = iterator.next();
			if (edge.getSource().equals(node) || edge.getTarget().equals(node)) {
				iterator.remove();
			}
		}
	}

	@Override
	public void removeEdge(E edge) throws InvalidEdgeException, EdgeNotExistsException {
		if (!this.edges.contains(edge))
			throw new EdgeNotExistsException(edge);
		// Not necessary...
		if (!this.nodes.contains(edge.getTarget()) || !this.nodes.contains(edge.getSource())) {
			throw new InvalidEdgeException("Invalid edge!", edge);
		}
		this.edges.remove(edge);
	}

	@Override
	public Collection<N> getPredecessors(N node) throws NodeNotExistsException {
		if (!this.nodes.contains(node))
			throw new NodeNotExistsException(node);
		ArrayList<N> precessors = new ArrayList<N>();
		for (E edge : this.edges) {
			if (edge.getTarget().equals(node)) {
				if (!precessors.contains(edge.getSource())) {
					precessors.add(edge.getSource());
				}
			}
		}
		return precessors;
	}

	@Override
	public Collection<E> getPredecessorEdges(N node) throws NodeNotExistsException {
		if (!this.nodes.contains(node))
			throw new NodeNotExistsException(node);
		ArrayList<E> precessorEdges = new ArrayList<E>();
		for (E edge : this.edges) {
			if (edge.getTarget().equals(node)) {
				if (!precessorEdges.contains(edge)) {
					precessorEdges.add(edge);
				}
			}
		}
		return precessorEdges;
	}

	@Override
	public Collection<N> getSuccessors(N node) throws NodeNotExistsException {
		if (!this.nodes.contains(node))
			throw new NodeNotExistsException(node);
		ArrayList<N> precessors = new ArrayList<N>();
		for (E edge : this.edges) {
			if (edge.getSource().equals(node)) {
				if (!precessors.contains(edge.getTarget())) {
					precessors.add(edge.getTarget());
				}
			}
		}
		return precessors;
	}

	@Override
	public Collection<E> getSuccessorEdges(N node) throws NodeNotExistsException {
		if (!this.nodes.contains(node))
			throw new NodeNotExistsException(node);
		ArrayList<E> precessorEdges = new ArrayList<E>();
		for (E edge : this.edges) {
			if (edge.getSource().equals(node)) {
				if (!precessorEdges.contains(edge)) {
					precessorEdges.add(edge);
				}

			}
		}
		return precessorEdges;
	}

	@Override
	public Collection<E> getEdgesBetween(N source, N target) throws NodeNotExistsException {
		if (!this.nodes.contains(source))
			throw new NodeNotExistsException(source);
		if (!this.nodes.contains(target))
			throw new NodeNotExistsException(target);
		ArrayList<E> edgesBetween = new ArrayList<E>();
		for (E edge : this.edges) {
			if (edge.getSource().equals(source) && edge.getTarget().equals(target)) {
				if (!edgesBetween.contains(edge)) {
					edgesBetween.add(edge);
				}
			}
		}
		return edgesBetween;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ID: " + this.id + "\n");
		Iterator<N> iterator = this.nodes.iterator();
		while (iterator.hasNext()) {
			N node = iterator.next();
			sb.append(node.toString());
		}
		Iterator<E> iteratorEdges = this.edges.iterator();
		while (iterator.hasNext()) {
			E edge = iteratorEdges.next();
			sb.append(edge.toString());
		}
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DiGraph other = (DiGraph) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}

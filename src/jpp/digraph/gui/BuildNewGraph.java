/**
 * 
 */
package jpp.digraph.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;

import jpp.digraph.graph.XYNode;
import jpp.digraph.io.XYGXLSupport;

/**
 * @author genbob
 *
 */
public class BuildNewGraph{
	private DiGraph<XYNode, CostEdge<XYNode>> graph;
	private HashMap<XYNode, Circle> nodes;
	private HashMap<CostEdge<XYNode>, ArrowPath> edges;
	private Scene scene;
	private double strech;
	private  final double DEFAULT_RADIUS = 30;
	public BuildNewGraph(File file, Scene scene)throws ParserConfigurationException{
		this.nodes = new HashMap<XYNode, Circle>();
		this.edges = new HashMap<CostEdge<XYNode>, ArrowPath>();
		this.scene = scene;
		try{
			FileInputStream fr = new FileInputStream(file);
			XYGXLSupport sup = new XYGXLSupport();
			this.graph = sup.read(fr);
		}catch(ParserConfigurationException | InvalidEdgeException | IOException | SAXException e){
			throw new ParserConfigurationException();		
		}
	}
	public DiGraph<XYNode, CostEdge<XYNode>> getGraph(){
		return this.graph;
	}
	public HashMap<XYNode, Circle> getNodes(){
		return this.nodes;
	}
	public HashMap<CostEdge<XYNode>, ArrowPath> getEdges(){
		return this.edges;
	}
	
	//calculate the borders of the given graph and the neccessary scretchings
	public void calculateBorder(AnchorPane pane){
		int xMin = Integer.MAX_VALUE;
		int xMax = Integer.MIN_VALUE;
		int yMin = Integer.MAX_VALUE;
		int yMax = Integer.MIN_VALUE;
		for (XYNode node : this.graph.getNodes()){
			if(node.getX() < xMin) xMin = node.getX();
			if(node.getX() > xMax) xMax = node.getX();
			if(node.getY() < yMin) yMin = node.getY();
			if(node.getY() > yMax) yMax = node.getY();
		}
		//scretching
		
		double xStrechCalc = 1.0;
		double yStrechCalc = 1.0;
		if(Integer.compareUnsigned(xMin, xMax) >0){
			xStrechCalc = (pane.getWidth()-pane.getLayoutX()) / xMin;
		}
		else{
			xStrechCalc = (pane.getWidth()-pane.getLayoutX()) / xMax;
		}
		if(Integer.compareUnsigned(yMin, yMax) >0){
			yStrechCalc = (pane.getHeight()-pane.getLayoutY())/ yMin;
		}
		else{
			yStrechCalc = (pane.getHeight()-pane.getLayoutY()) / yMax;
		}
		this.strech = (xStrechCalc<yStrechCalc) ? xStrechCalc : yStrechCalc;
	}

	
	public void buildGraph() {
		AnchorPane pane = (AnchorPane) scene.lookup("#pane");
		calculateBorder(pane);
		pane = addNodes(pane);
		pane = addEdges(pane);
	}
	
	public AnchorPane addNodes(AnchorPane pane){
		for (XYNode node : this.graph.getNodes()){
			
			pane.getChildren().addAll(getNewNode(node),addId(node));
		}
		
		return pane;
	}
	public Label addId(XYNode node){
		Label id = new Label(node.getId());
		id.setScaleX(1);
		id.setScaleY(1);
		id.relocate((node.getX())*this.strech,(node.getY()+10)*this.strech);
		return id;
	}
	
	public Circle getNewNode(XYNode node){
		Circle circle = new Circle(this.DEFAULT_RADIUS*this.strech);
		circle.setFill(Color.AQUA);
		circle.setCenterX(node.getX()*this.strech);
		circle.setCenterY(node.getY()*this.strech);
		this.nodes.put(node,circle);
		return circle;
		
	}
	public AnchorPane addEdges(AnchorPane pane){
		for (CostEdge<XYNode> edge : this.graph.getEdges()){
			ArrowPath path = getNewEdge(edge);
			pane.getChildren().addAll(path.getPath(), path.getArrow());
			
		}
		return pane;
	}
	public ArrowPath getNewEdge(CostEdge<XYNode> edge){
		ArrowPath line = new ArrowPath(edge.getSource(),
				edge.getTarget(),
				this.strech);
		this.edges.put(edge, line);

		return line;
	}
	
	
	
	

}

/**
 * 
 */
package jpp.digraph.gui;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.xml.parsers.ParserConfigurationException;



import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;

import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.IXYNode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.search.BFS;
import jpp.digraph.search.DFS;
import jpp.digraph.search.Dijkstra;
import jpp.digraph.search.IDiGraphSearch;
import jpp.digraph.search.XYAStar;

/**
 * @author genbob
 *
 */
public class MenueController<G extends IDiGraph<N, E>, N extends IXYNode, E extends ICostEdge<N>>
		implements Initializable {
	public DiGraphGUI main;
	private N startNode;
	private N endNode;
	private String suchAlgo;
	private BuildNewGraph bg;
	private double speedValue = 1;
	private SearchThread<G, N, E> st;
	@FXML
	private Button openDataButton;
	@FXML
	private Button playButton;
	@FXML
	private Button stopButton;
	@FXML
	private Button pauseButton;
	@FXML
	private Slider speed;
	@FXML
	private ComboBox<String> startComboBox;
	@FXML
	private ComboBox<String> zielComboBox;
	@FXML
	private ComboBox<String> suchAlgoComboBox;
	@FXML
	private Slider geschSlider;
	@FXML
	private AnchorPane pane;
	private DiGraph<XYNode, CostEdge<XYNode>> graph;

	public void setMain(DiGraphGUI main) {
		this.main = main;
	}

	// import button
	@FXML
	public void openData() {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Graph");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("*.gxl", "*.gxl"),
				new FileChooser.ExtensionFilter("*.xml", "*.xml"));
		try{
			File file = fileChooser.showOpenDialog(this.main.getStage());
			resetSettings();
			this.bg = new BuildNewGraph(file, this.main.getStage().getScene());
			bg.buildGraph();
			this.graph = bg.getGraph();
		} catch(NullPointerException  | ParserConfigurationException p){
			
		}
		refreshComboBox();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.playButton.setText("\u25B6");
		this.pauseButton.setText("\u23F8");
		this.stopButton.setText("\u25A0");
		fillComboBoxSearch();
	}

	public void onPlay() {
		if (st != null && this.st.getPause()) {
			this.st.pause(false);
			this.pauseButton.setDisable(false);
		} else {
			startSearch();
		}
		this.playButton.setDisable(true);
		this.pauseButton.setDisable(false);
		this.stopButton.setDisable(false);
		this.startComboBox.setDisable(true);
		this.zielComboBox.setDisable(true);
		this.suchAlgoComboBox.setDisable(true);
		this.openDataButton.setDisable(true);
	}

	public void onPause() {
		if (!st.getPause()) {
			st.pause(true);
			this.pauseButton.setDisable(true);
			this.playButton.setDisable(false);
		}
	}

	public void onStop() {
		this.st.stop();
		this.stopButton.setDisable(true);
		this.pauseButton.setDisable(true);
		resetSettings();
		this.playButton.setDisable(false);
		this.openDataButton.setDisable(false);
		refreshComboBox();

	}
	public void ready(List<CostEdge<XYNode>> list){
		for(CostEdge<XYNode> edge : list){
			this.bg.getEdges().get(edge).setColor(Color.GREENYELLOW);
			this.pauseButton.setDisable(true);
			this.playButton.setDisable(true);
			
		}
	}

	private void refreshComboBox() {
		if (this.startComboBox.getItems().isEmpty()) {
			for (XYNode node : this.graph.getNodes()) {
				this.startComboBox.getItems().add(node.getId());
				this.zielComboBox.getItems().add(node.getId());
			}

		}
		if(this.startComboBox.getValue() != null && 
				!this.startComboBox.getValue().equals("Select")){
			this.bg.getNodes().get(this.graph.getNodeById(startComboBox.getValue())).setFill(Color.GREEN);
			this.startNode = (N) this.graph.getNodeById(this.startComboBox.getValue());
		}
		if(this.zielComboBox.getValue() != null &&
				!this.zielComboBox.getValue().equals("Select")){
			this.bg.getNodes().get(this.graph.getNodeById(zielComboBox.getValue())).setFill(Color.RED);
			this.endNode = (N) this.graph.getNodeById(this.zielComboBox.getValue());
		}
		this.startComboBox.setDisable(false);
		this.startComboBox.setPromptText("Select");
		this.zielComboBox.setPromptText("Select");
		this.zielComboBox.setDisable(false);
		this.suchAlgoComboBox.setDisable(false);
		this.startComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				if (startNode != null) {
					bg.getNodes().get(startNode).setFill(Color.AQUA);
				}
				startNode = (N) graph.getNodeById(arg2);
				bg.getNodes().get(startNode).setFill(Color.GREEN);
				checkReady();
			}
		});
		this.zielComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				if (endNode != null) {
					bg.getNodes().get(endNode).setFill(Color.AQUA);
				}
				endNode = (N) graph.getNodeById(arg2);
				bg.getNodes().get(endNode).setFill(Color.RED);
				checkReady();

			}
		});

	}

	public void fillComboBoxSearch() {
		this.suchAlgoComboBox.setPromptText("Select");
		this.suchAlgoComboBox.getItems().addAll("DFS", "BFS", "A*", "Dijkstra");
		this.suchAlgoComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				suchAlgo = arg2;
				checkReady();
			}
		});
		this.speed.setDisable(false);
		this.speed.setMin(0.1);
		this.speed.setMax(2.0);
		this.speed.setValue(1.0);
		this.speed.setShowTickLabels(true);
		this.speed.setShowTickMarks(true);
		this.speed.setMinorTickCount(1);
		this.speed.setMajorTickUnit(1.0);
		this.speed.setBlockIncrement(0.1);
		this.speed.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0, Number arg1, Number arg2) {
				speedValue = arg2.doubleValue();
			}
		});
	}

	public boolean checkReady() {
		if (this.graph != null && this.startNode != null && this.endNode != null && this.suchAlgo != null) {
			this.playButton.setDisable(false);
			return true;
		} else {
			this.playButton.setDisable(true);

			return false;
		}

	}

	public void startSearch() {
		IDiGraphSearch<G, N, E> search;
		if (suchAlgo.equals("DFS")) {
			search = new DFS<G, N, E>();
		} else if (suchAlgo.equals("BFS")) {
			search = new BFS<G, N, E>();
		} else if (suchAlgo.equals("A*")) {
			search = new XYAStar<G, N, E>();
		} else {
			search = new Dijkstra<G, N, E>();
		}
		this.st = new SearchThread(graph, bg, search, startNode, endNode, speedValue, this);
	}

	// if a new graph is imported, reset settings
	private void resetSettings() {
		
		this.speed.setValue(1.0);
		if(this.graph != null){
			for(INode node : this.graph.getNodes()){
				this.bg.getNodes().get(node).setFill(Color.AQUA);
			}
			for(CostEdge<XYNode> edge : this.graph.getEdges()){
				this.bg.getEdges().get(edge).setColor(Color.GRAY);
			}
		}
		this.st = null;
		this.startNode = null;
		this.endNode = null;
		this.playButton.setDisable(true);
		this.pauseButton.setDisable(true);
		this.stopButton.setDisable(true);
		this.startComboBox.setDisable(true);
		this.zielComboBox.setDisable(true);
		this.suchAlgoComboBox.setDisable(true);

	}

}

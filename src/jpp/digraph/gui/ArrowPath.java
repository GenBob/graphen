/**
 * 
 */
package jpp.digraph.gui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import jpp.digraph.graph.XYNode;

/**
 * @author genbob
 *
 */
public class ArrowPath {

	private XYNode start;
	private XYNode end;
	private double strech;
	private Path path;
	private Path arrow;
	public ArrowPath(XYNode start, XYNode end, double strech){
		
		this.start = start;
		this.end = end;
		this.strech = strech;
		
		double[] arrow = calcCoord();
		this.path = createPath(arrow);
		this.arrow = createArrow(arrow);
	}
	public Path getPath(){
		return this.path;
	}
	public Path getArrow(){
		return this.arrow;
	}
	public void setColor(Color color){
		this.path.setStroke(color);
		if(color == Color.GRAY) this.path.setStrokeWidth(1);
		else{
			this.path.setStrokeWidth(8*this.strech);
		}
		
	}
	
	public Path createPath(double[] end){
		
		Path start = new Path();
		MoveTo moveTo = new MoveTo();
		moveTo.setX(this.start.getX()*this.strech);
		moveTo.setY(this.start.getY()*this.strech);
		
		LineTo lineTo = new LineTo();
		lineTo.setX(end[0]*this.strech);
		lineTo.setY(end[1]*this.strech);
		start.getElements().addAll(moveTo,lineTo);
		start.setStroke(Color.GRAY);
		return start;

	}
	public Path createArrow(double[] end){
		Path arrow = new Path();
		MoveTo arroM = new MoveTo();
		arroM.setX(end[0] * this.strech);
		arroM.setY(end[1]*this.strech);
		LineTo arroL = new LineTo();
		arroL.setX(this.end.getX()*this.strech);
		arroL.setY(this.end.getY()*this.strech);
		arrow.getElements().addAll(arroM,arroL);
		arrow.setStroke(Color.BLACK);
		arrow.setStrokeWidth(4*this.strech);
		return arrow;
	}
	
	public double[] calcCoord(){
		int xEnd = this.end.getX();
		int yEnd = this.end.getY();
		int xStart = this.start.getX();
		int yStart = this.start.getY();
		int diffX = xEnd-xStart;
		int diffY = yEnd-yStart;
		double x = diffX * 0.9 + xStart;
		double y = diffY * 0.9 + yStart;
		double[] out = {x,y};
		return out;
	}
	
	
	
	

}

/**
 * 
 */
package jpp.digraph.gui;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
/**
 * @author genbob
 *
 */
public class DiGraphGUI extends Application{

	private Stage primaryStage;
	public static void main(String[] args) {
		launch(args);
	}
	public Stage getStage(){
		return this.primaryStage;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		this.primaryStage.setResizable(false);
		this.primaryStage.setTitle("Graph");
		
		mainWindow();
		
		
	}
	public void mainWindow(){
		try{
			
			FXMLLoader loader = new FXMLLoader(DiGraphGUI.class.getResource("Test.fxml"));
			BorderPane borderPane = loader.load();
			Scene scene = new Scene(borderPane);
			
			MenueController<?, ?, ?> menueController = loader.getController();
			menueController.setMain(this);
			
			this.primaryStage.setScene(scene);
			this.primaryStage.show();
		} catch (Exception e){
			System.out.println("Error in mainWindow; " + e.getMessage());
		}
	}
}

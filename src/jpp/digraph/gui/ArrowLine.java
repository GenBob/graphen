/**
 * 
 */
package jpp.digraph.gui;

import javax.swing.plaf.synth.SynthSplitPaneUI;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import jpp.digraph.graph.XYNode;

/**
 * @author genbob
 *
 */
public class ArrowLine extends Line {

	private XYNode start;
	private XYNode end;
	private double radius;
	private double strech;
	private double xDiff;
	private double yDiff;
	private double winkel;
	private double hypo;

	public ArrowLine(XYNode start, XYNode end, double strech, double radius) {
		super();
		this.start = start;
		this.end = end;
		this.strech = strech;
		this.radius = radius;
		this.xDiff = Math.abs(start.getX() - end.getX());
		this.yDiff = Math.abs(start.getY() - end.getY());
		this.winkel = Math.atan(this.yDiff/this.xDiff);
		this.hypo = Math.sqrt(Math.pow(yDiff, 2)+Math.pow(xDiff, 2));
		System.out.println(hypo);
		createPath();
		setStroke(Color.GRAY);
//		System.out.println("XStart: " + (this.start.getX() * this.strech + relaxX()) + " YStart: "
//				+ (this.start.getY() * this.strech + relaxY()));
//		System.out.println("XEnd: " + (this.end.getX() * this.strech + relaxX()) + " " + " YEnd: "
//				+ (this.end.getY() * this.strech + relaxY()));
//		System.out.println("XNew: " + calcDiffX() + " YNew: " + calcDiffY());
//		System.out.println();
	}

//	public double relaxX() {
//		return Math.sin(Math.atan(this.xDiff / this.yDiff) * (this.radius * this.strech));
//	}
//
//	public double relaxY() {
//		return Math.cos(Math.atan(this.xDiff / this.yDiff) * (this.radius * this.strech));
//
//	}

	public void createPath() {
		double calcX = 1;
		double calcY = 1;
		//fallend
		if(this.start.getY() > this.end.getY() && this.start.getX()<this.end.getX()){
			calcX = -calcX();
			calcY = -calcY();
		}
		else if(this.start.getY() > this.end.getY() && this.start.getX()>this.end.getX()){
			calcX = calcX();
			calcY = calcY();
		}
		//steigend
		else if(this.start.getY() < this.end.getY() && this.start.getX()<this.end.getX()){
			calcX = -calcX();
			calcY = -calcY();
		}
		else{
			calcX = calcX();
			calcY = calcY();
		}
			
		
		setStartX(this.start.getX() * this.strech);
		setStartY(this.start.getY() * this.strech);

		setEndX((this.end.getX() +calcX)* this.strech);
		setEndY((this.end.getY() +calcY)* this.strech);
		System.out.println(this.start.getX()*this.strech +" " + this.start.getY()*this.strech+ " ; " + this.end.getX()*this.strech + " " + this.end.getY()*this.strech
		+ " calcX: " + calcX *this.strech+ " calcY " + calcY*this.strech);

	}
	public double calcX(){
		return Math.cos(this.winkel) * 0.2* this.hypo;
	}
	public double calcY(){
		return Math.sin(this.winkel) * 0.2* this.hypo;
	}

	
}

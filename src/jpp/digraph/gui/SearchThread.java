/**
 * 
 */
package jpp.digraph.gui;

import java.util.List;

import javafx.scene.paint.Color;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.IXYNode;
import jpp.digraph.search.IDiGraphSearch;
import jpp.digraph.search.IDiGraphSearchListener;

/**
 * @author genbob
 *
 */
public class SearchThread<G extends IDiGraph<N, E>, N extends IXYNode, E extends ICostEdge<N>> {

	private G graph;
	private BuildNewGraph bg;
	private double speed;
	private IDiGraphSearchListener<N, E> listener;
	private IDiGraphSearch<G, N, E> search;
	private INode start;
	private INode finish;
	private List<E> list;
	private Thread th;
	private boolean pause = false;
	private boolean stopped = false;
	private MenueController menue;
	public SearchThread(G graph2,
			BuildNewGraph bg,
			IDiGraphSearch<G,N,E> search,
			N start,
			N finish,
			double speedValue,
			MenueController menue){
		this.search = search;
		this.start = start;
		this.finish = finish;
		this.graph = graph2;
		this.bg = bg; 
		this.speed = speedValue;
		this.menue = menue;
		this.listener = new IDiGraphSearchListener<N, E>() {
			
			

			@Override
			public void onExpandNode(N node, List<E> way) {
				try {
					
						Thread.sleep((long) speedValue*1000);
						synchronized (this){
							while(pause ){
								Thread.yield();
								if(stopped){
									break;
								}
							}
						
					}
					
					
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					
				}
				bg.getNodes().get(node).setFill(Color.GREEN);
				way.forEach(edge->{
					bg.getEdges().get(edge).setColor(Color.ORANGE);
				});
			};
		};
				
			
		
		Runnable run = new Runnable(){
			@Override
			public void run() {
				try{
						list = search.search(graph, start, finish);
						ready(list);

					
				}catch(Exception e){
					
				}
		
			}
		};
			
		
		this.search.addListener(listener);
		th = new Thread(run);
		th.start();
		
		
		
		
		
	}
	public void ready(List<E> list){
		this.menue.ready(list);
	}
	
	public synchronized void setSpeed(double speed){
		this.speed = speed;
		notify();
	}
	public synchronized void pause(boolean pause){
		this.pause = pause;
		notify();
	}
	public boolean getPause(){
		return this.pause;
	}
	public void stop(){
		this.th.interrupt();
	}
	
	
	

}

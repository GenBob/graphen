/**
 * 
 */
package jpp.digraph.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;

/**
 * @author genbob
 *
 */
public abstract class GXLSupport<G extends IDiGraph<N, E>, N extends INode, E extends IEdge<N>>
		implements IGXLSupport<G, N, E> {
	public G newGraph;
	
	
	public G read(InputStream in) 
			throws ParserConfigurationException, IOException, SAXException, InvalidEdgeException{
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		
		Document doc = builder.parse(in);
		
		Node root = doc.getDocumentElement();
		NodeList list = root.getChildNodes();
		this.newGraph = createGraph();
		newGraph.setId(root.getFirstChild().getAttributes().getNamedItem("id").toString());
		
		for (int i = 0;i<list.getLength();i++){

			NodeList list2 = list.item(i).getChildNodes();
			for (int j = 0; j<list2.getLength(); j++){
				if(list2.item(j).getNodeName().equals("node")){
					newGraph.addNode(createNode(list2.item(j)));
				}
				else if(list2.item(j).getNodeName().equals("edge")){
					newGraph.addEdge(createEdge(list2.item(j)));
				}
				
			}
		}
		return newGraph;
		
	}
	public void write(G graph, OutputStream os) 
			throws ParserConfigurationException,
			IOException,
			TransformerConfigurationException,
			TransformerException{
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		Element root = doc.createElement("gxl");
		doc.appendChild(root);
		Collection<N> nodes = graph.getNodes();
		Collection<E> edges = graph.getEdges();
		Element graphEle = doc.createElement("graph");
		graphEle.setAttribute("id", graph.getId());
		root.appendChild(graphEle);
		
		nodes.forEach(node-> {
			graphEle.appendChild(createElement(doc,node));
		});
		
		edges.forEach(edge -> {
			graphEle.appendChild(createElement(doc,edge));
		});
		DOMSource domSource = new DOMSource(doc);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
		transformer.transform(domSource, new StreamResult(os));
		
	}
	public abstract G createGraph();
	public abstract N createNode(org.w3c.dom.Node element);
	public abstract E createEdge(org.w3c.dom.Node element);
	public abstract org.w3c.dom.Element createElement(org.w3c.dom.Document doc, N node);
	public abstract org.w3c.dom.Element createElement(org.w3c.dom.Document doc, E edge);
	

}

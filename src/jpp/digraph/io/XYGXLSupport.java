/**
 * 
 */
package jpp.digraph.io;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;

/**
 * @author genbob
 *
 */
public class XYGXLSupport extends GXLSupport<DiGraph<XYNode, CostEdge<XYNode>>, XYNode, CostEdge<XYNode>> {
	private Map<String, XYNode> nodes;
	
	public XYGXLSupport() {
		
	}

	@Override
	public DiGraph<XYNode, CostEdge<XYNode>> createGraph() {
		this.nodes = new HashMap<String, XYNode>();
		return new DiGraph<XYNode, CostEdge<XYNode>>();
	}

	@Override
	public XYNode createNode(Node element) {
		Element local = (Element) element;
		String id = local.getAttribute("id");
		
		NodeList list = local.getChildNodes();
		String discription = list.item(0).getFirstChild().getTextContent();
		int x = Integer.parseInt(list.item(1).getFirstChild().getTextContent());
		int y = Integer.parseInt(list.item(2).getFirstChild().getTextContent());
		
		XYNode newNode = new XYNode(id, x, y, discription);
		this.nodes.put(id, newNode);
		return newNode;
		
	}

	@Override
	public CostEdge<XYNode> createEdge(Node element) {
		Element root = (Element) element;
		String from = root.getAttribute("from");
		String id = root.getAttribute("id");
		String to = root.getAttribute("to");
		
		String description = root.getChildNodes().item(0).getFirstChild().getTextContent();
		double cost = Double.valueOf(root.getChildNodes().item(1).getFirstChild().getTextContent());
		
		return new CostEdge<XYNode>(id, this.nodes.get(from), this.nodes.get(to), cost, description);
	}

	@Override
	public Element createElement(Document doc, XYNode node) {
		Element out = doc.createElement("node");
		out.setAttribute("id", node.getId());
		
		Element attrDescription = doc.createElement("attr");
		attrDescription.setAttribute("name", "description");
		Element descriptionString = doc.createElement("string");
		descriptionString.setTextContent(node.getDescription());
		attrDescription.appendChild(descriptionString);
		
		Element attrX = doc.createElement("attr");
		attrX.setAttribute("name", "x");
		Element xInt = doc.createElement("int");
		xInt.setTextContent(String.valueOf(node.getX()));
		attrX.appendChild(xInt);
		
		Element attrY = doc.createElement("attr");
		attrY.setAttribute("name", "y");
		Element yInt = doc.createElement("int");
		yInt.setTextContent(String.valueOf(node.getY()));
		attrY.appendChild(yInt);
		
		out.appendChild(attrDescription);
		out.appendChild(attrX);
		out.appendChild(attrY);
		
		return out;
	}

	@Override
	public Element createElement(Document doc, CostEdge<XYNode> edge) {
		Element out = doc.createElement("edge");
		out.setAttribute("from", edge.getSource().getId());
		out.setAttribute("id", edge.getId());
		out.setAttribute("to", edge.getTarget().getId());
		
		Element attrDescription = doc.createElement("attr");
		attrDescription.setAttribute("name", "description");
		Element descriptionString = doc.createElement("string");
		descriptionString.setTextContent(edge.getDescription());
		attrDescription.appendChild(descriptionString);
		
		Element attrCost = doc.createElement("attr");
		attrCost.setAttribute("name", "cost");
		Element costFloat = doc.createElement("float");
		costFloat.setTextContent(String.valueOf(edge.getCost()));
		attrCost.appendChild(costFloat);
		
		out.appendChild(attrDescription);
		out.appendChild(attrCost);		
		
		return out;
	}

	
}

/**
 * 
 */
package jpp.digraph.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.INode;

/**
 * @author genbob
 *
 */
public abstract class AbstractAStar<G extends IDiGraph<N, E>, N extends INode, E extends ICostEdge<N>>
		implements IDiGraphSearch<G, N, E> {
	protected Collection<IDiGraphSearchListener<N, E>> listener;
	protected TreeSet<N> open;
	protected ArrayList<N> openBack;
	protected TreeSet<N> close;
	protected N target;
	protected HashMap<N, Double> cost;
	protected HashMap<N,ArrayList<E>> preccesors;
	protected Comparator<N> comparator;
	public AbstractAStar(){
		
		this.comparator = new Comparator<N>(){

			@Override
			public int compare(N o1, N o2) {
				
				double one = cost.get(o1) - h(o1, target);
				double two = cost.get(o2) - h(o2, target);
				int comp=  Double.compare(one, two);
				if (comp != 0) return comp;
				comp = o1.getId().compareTo(o2.getId());
				if (comp != 0) return comp;
				comp = o1.getDescription().compareTo(o2.getDescription());
				return comp;
			}
			
		};
		this.listener = new ArrayList<IDiGraphSearchListener<N, E>>();
		
	}
	@Override
	public List<E> search(G graph, N source, N target) throws NodeNotExistsException {
		this.openBack = new ArrayList<N>();
		this.cost = new HashMap<N, Double>();
		double start = 0;
		for (N node : graph.getNodes()){
			this.cost.put(node, start);
		}
		this.preccesors = new HashMap<N,ArrayList<E>>();
		this.open = new TreeSet<N>(this.comparator.reversed());
		this.close = new TreeSet<N>(this.comparator);
		if(source == null) throw new NodeNotExistsException(source);
		if(target == null) throw new NodeNotExistsException(target);
		if(!graph.getNodes().contains(source)) throw new NodeNotExistsException(source);
		if(!graph.getNodes().contains(target)) throw new NodeNotExistsException(target);
		this.target = target;
		open.add(source);
		openBack.add(source);
		this.preccesors.put(source, new ArrayList<E>());
		while(!open.isEmpty()){
			N node = open.pollFirst();
			openBack.remove(node);
			expandNode(node);
			if(node.equals(target)){
				return this.preccesors.get(node);
			}
			close.add(node);
			for (E edge : graph.getSuccessorEdges(node)){
				N sucNode = edge.getTarget();
				

				
				if(!open.contains(sucNode) && !close.contains(sucNode) && !openBack.contains(sucNode)){
					open.add(sucNode);
					openBack.add(sucNode);
					this.cost.put(sucNode, this.cost.get(node) + edge.getCost());
					ArrayList<E> newList = new ArrayList<E>();
					newList.addAll(this.preccesors.get(node));
					newList.add(edge);
					this.preccesors.put(sucNode, newList);
				}
				else if(this.cost.get(node) + edge.getCost() < this.cost.get(sucNode)){
					this.cost.put(sucNode, this.cost.get(node) + edge.getCost());
					ArrayList<E> newList = new ArrayList<E>();
					newList.addAll(this.preccesors.get(node));
					newList.add(edge);
					this.preccesors.put(sucNode, newList);
					if(close.contains(sucNode)){
						open.add(sucNode);
						openBack.add(sucNode);
						close.remove(sucNode);
					}
				}
			}
			
		}
		return null;
	}
	public void expandNode(N node){
		if(getListeners() == null) return;
		getListeners().forEach(listener ->{
			listener.onExpandNode(node, this.preccesors.get(node));
		});
	}
	

	@Override
	public void addListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.add(listener);
		
	}

	@Override
	public void removeListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.remove(listener);
		
	}

	@Override
	public Collection<IDiGraphSearchListener<N, E>> getListeners() {
		return this.listener;
	}
	public abstract double h(N node, N target);
	
	

}	


/**
 * 
 */
package jpp.digraph.search;

import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.IXYNode;

/**
 * @author genbob
 *
 */
public class XYAStar<G extends IDiGraph<N, E>, N extends IXYNode, E extends ICostEdge<N>>
		extends AbstractAStar<G, N, E> {

	
	@Override
	public double h(N node, N target) {		
		return  Math.sqrt(
				Math.pow(node.getX()-target.getX(), 2) 
				+ Math.pow(node.getY()-target.getY(), 2));
	}

}

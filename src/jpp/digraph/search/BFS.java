/**
 * 
 */
package jpp.digraph.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ArrayBlockingQueue;

import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.INode.State;

/**
 * @author genbob
 *
 */
public class BFS<G extends IDiGraph<N, E>, N extends INode, E extends ICostEdge<N>> implements IDiGraphSearch<G, N, E> {
	private Map<N,ArrayList<E>> preccessorEdges;
	private Collection<IDiGraphSearchListener<N, E>> listener;
	private Map<N,State> state;
	public BFS(){
		this.listener = new ArrayList<IDiGraphSearchListener<N, E>>();

	}
	@Override
	public List<E> search(G graph, N source, N target) throws NodeNotExistsException {
		this.state = new HashMap<N,State>();
		this.preccessorEdges = new HashMap<N,ArrayList<E>>();
		if(source == null) throw new NodeNotExistsException(source);
		if(target == null) throw new NodeNotExistsException(target);
		graph.getNodes().forEach(node ->{
			this.state.put(node, State.UNKNOWN);
		});
		if(!graph.getNodes().contains(source)) throw new NodeNotExistsException(source);
		if(!graph.getNodes().contains(target)) throw new NodeNotExistsException(target);
		ArrayBlockingQueue<N> queue = new ArrayBlockingQueue<N>(graph.getNodeCount());
		this.state.put(source, State.IN_WORK);
		this.preccessorEdges.put(source, new ArrayList<E>());
		queue.add(source);
		while(!queue.isEmpty()){
			N node = queue.remove();
			expandNode(node);
			if(node.equals(target)){
				
				return this.preccessorEdges.get(node);
			}
			Iterator<E> iterator = graph.getSuccessorEdges(node).iterator();
			while (iterator.hasNext()){
				E edge = iterator.next();
				if(this.state.get(edge.getTarget()) == State.IN_WORK ||
						this.state.get(edge.getTarget()) == State.READY) continue;
				N nodeFound = edge.getTarget();
				ArrayList<E> newList = new ArrayList<E>();
				newList.addAll(this.preccessorEdges.get(node));
				newList.add(edge);
				this.preccessorEdges.put(nodeFound, newList);
				this.state.put(nodeFound, State.IN_WORK);
				queue.offer(nodeFound);
			}
			this.state.put(node, State.READY);
		}
		return null;
	}
	public void expandNode(N node){
		if(getListeners() == null) return;
		getListeners().forEach(listener ->{
			listener.onExpandNode(node, this.preccessorEdges.get(node));
		});
	}
	

	@Override
	public void addListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.add(listener);
		
	}

	@Override
	public void removeListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.remove(listener);
		
	}

	@Override
	public Collection<IDiGraphSearchListener<N, E>> getListeners() {
		return this.listener;
	}

}

package jpp.digraph.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Stack;

import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.INode.State;

/**
 * @author genbob
 *
 */
public class DFS<G extends IDiGraph<N, E>, N extends INode, E extends ICostEdge<N>> implements IDiGraphSearch<G, N, E> {
	private Map<N,ArrayList<E>> preccessor;
	private Collection<IDiGraphSearchListener<N, E>> listener;
	private Map<N,State> state;

	public DFS(){
		this.listener = new ArrayList<IDiGraphSearchListener<N, E>>();

	}
	@Override
	public List<E> search(G graph, N source, N target) throws NodeNotExistsException {
		this.state = new HashMap<N,State>();
		this.preccessor = new HashMap<N, ArrayList<E>>();
		if(source == null) throw new NodeNotExistsException(source);
		if(target == null) throw new NodeNotExistsException(target);
		if(!graph.getNodes().contains(source)) throw new NodeNotExistsException(source);
		if(!graph.getNodes().contains(target)) throw new NodeNotExistsException(target);
		graph.getNodes().forEach(node ->{
			this.state.put(node, State.UNKNOWN);
		});
		Stack<N> stack = new Stack<N>();
		stack.push(source);
		this.preccessor.put(source, new ArrayList<E>());
		while(!stack.isEmpty()){
			N node = stack.peek();
			
			if(node.equals(target)){
				expandNode(node);
				return this.preccessor.get(node);
			}
			if(!checkNeihg(node, graph)){
				if(this.state.get(node) == State.UNKNOWN) expandNode(node);
				state.put(node, State.READY);
				stack.pop();
				continue;
			}
			if(this.state.get(node) == State.UNKNOWN){
				expandNode(node);
			}
			
			
			this.state.put(node, State.IN_WORK);
			
			if(node.equals(target)){
				return this.preccessor.get(node);
			}
			Iterator<E> iterator = graph.getSuccessorEdges(node).iterator();
			if(!iterator.hasNext()){
				this.state.put(node, State.READY);
				stack.pop();
				continue;
			}
			while (iterator.hasNext()){
				
				E edge = iterator.next();
				N nodeFound = edge.getTarget();
				if(this.state.get(nodeFound) == State.IN_WORK ||
						this.state.get(nodeFound) == State.READY){
					if(!iterator.hasNext()){
						this.state.put(node, State.READY);
						stack.pop();
						break;
					}
					continue;
				}
				
				stack.push(nodeFound);
//				this.state.put(nodeFound, State.IN_WORK);
				ArrayList<E> newList = new ArrayList<E>();
				newList.addAll(this.preccessor.get(node));
				newList.add(edge);
				this.preccessor.put(nodeFound, newList);
				break;
			}
			
			
		}
		return null;
	}
	public boolean checkNeihg(N node, G graph) throws NodeNotExistsException{
		boolean out = false;
		for(N sucNode : graph.getSuccessors(node)){
			if(this.state.get(sucNode) == State.UNKNOWN) out = true;
		}
		return out;
	}
	public void expandNode(N node){
		if(getListeners() == null) return;
		getListeners().forEach(listener ->{
			listener.onExpandNode(node, this.preccessor.get(node));
		});
	}

	@Override
	public void addListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.add(listener);
		
	}

	@Override
	public void removeListener(IDiGraphSearchListener<N, E> listener) {
		this.listener.remove(listener);
		
	}

	@Override
	public Collection<IDiGraphSearchListener<N, E>> getListeners() {
		return this.listener;
	}

}

/**
 * 
 */
package test;

import java.util.ArrayList;
import java.util.Collection;

import jpp.digraph.exceptions.EdgeNotExistsException;
import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import junit.framework.TestCase;
import junit.textui.TestRunner;

/**
 * @author genbob
 *
 */
public class TestGraph extends TestCase{
	INode test1;
	INode test2;
	INode test3;
	INode test4;
	INode test5;
	INode test6;
	INode test7;
	IEdge<INode> edge1;
	IEdge<INode> edge2;
	IEdge<INode> edge3;
	IEdge<INode> edge4;
	IEdge<INode> edge5;
	IEdge<INode> edge6;
	
	private DiGraph<INode, IEdge<INode>> graph;
	
	public TestGraph() throws InvalidEdgeException{
		Collection<INode> nodes = new ArrayList<INode>();
		Collection<IEdge<INode>> edges = new ArrayList<IEdge<INode>>();
		test1 = new XYNode("test1", 1, 0);
		test2 = new XYNode("test2", 0, 1);
		test3 = new XYNode("test3", 1, 1);
		test4 = new XYNode("test4", 2, 1);
		 test5 = new XYNode("test5", 0, 2);
		 test6 = new XYNode("test6", 1, 2);
		 test7 = new XYNode("test7", 2, 2);
		 edge1 = new CostEdge<INode>("e1", test1, test2, 1);
		 edge2 = new CostEdge<INode>("e1", test1, test3, 1);
		edge3 = new CostEdge<INode>("e3", test1, test4, 1);
		 edge4 = new CostEdge<INode>("e4", test2, test5, 1);
		 edge5 = new CostEdge<INode>("e5", test3, test6, 1);
		 edge6 = new CostEdge<INode>("e6", test4, test7, 1);

		nodes.add(test1);
		nodes.add(test2);
		nodes.add(test3);
		nodes.add(test4);
		nodes.add(test5);
		nodes.add(test6);
		nodes.add(test7);

		edges.add(edge1);
		edges.add(edge2);
		edges.add(edge3);
		edges.add(edge4);
		edges.add(edge5);
		edges.add(edge6);
		this.graph = new DiGraph<INode, IEdge<INode>>(nodes, edges);
	}
	
	public void testContainsEdge() throws InvalidEdgeException, EdgeNotExistsException, NodeNotExistsException{
		
		
		
		
		assertEquals(true, this.graph.containsEdge(edge1));
		assertEquals(true, this.graph.containsEdge(edge2));
		assertEquals(true, this.graph.containsEdge(edge3));
		assertEquals(5, this.graph.getEdgeCount());
		this.graph.removeEdge(edge1);
		assertEquals(4, this.graph.getEdgeCount());
		this.graph.addEdge(edge1);
		assertEquals(5, this.graph.getEdgeCount());
		

		
		assertEquals(7, this.graph.getNodeCount());
		this.graph.removeNode(test1);
		assertEquals(6, this.graph.getNodeCount());
		this.graph.addNode(test1);
		assertEquals(7, this.graph.getNodeCount());
		this.graph.addNode(test1);
		assertEquals(7, this.graph.getNodeCount());
		

	
	
	}
	public void testEqual(){
		assertEquals(true, test1.equals(test1));
		assertEquals(true, test2.equals(test2));
		assertEquals(false, test1.equals(test2));
		assertEquals(true, edge1.equals(edge1));
		assertEquals(true, edge1.equals(edge2));
	}
	
	public static void main(String[] args) {
		TestRunner.run(TestGraph.class);
		

	}

}

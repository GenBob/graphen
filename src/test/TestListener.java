/**
 * 
 */
package test;

import java.util.List;

import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.search.IDiGraphSearchListener;

/**
 * @author genbob
 *
 */
public class TestListener implements IDiGraphSearchListener<INode, CostEdge<INode>> {

	@Override
	public void onExpandNode(INode n, List<CostEdge<INode>> way) {
		System.out.println(n.getId());
		
	}

}

/**
 * 
 */
package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.io.XYGXLSupport;
import jpp.digraph.search.BFS;
import jpp.digraph.search.IDiGraphSearchListener;

/**
 * @author genbob
 *
 */
public class Main {
	static Thread th;
	public static void main(String[] args) throws Exception {
		IDiGraphSearchListener<INode, IEdge<INode>> listener = new IDiGraphSearchListener<INode, IEdge<INode>>() {
			
			@Override
			public void onExpandNode(INode node, List<IEdge<INode>> way) {
				try {
					update(node);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		};
		
		XYGXLSupport sup = new XYGXLSupport();
		FileInputStream in = new FileInputStream(new File("sample.gxl"));
		DiGraph<XYNode, CostEdge<XYNode>> graph = sup.read(in);
		BFS bfs = new BFS();
		bfs.addListener(listener);
//		TaskTest task = new TaskTest(bfs,graph);
//		task.start(null);
		Runnable run = new Runnable(){

			@Override
			public void run() {
				try {
					bfs.search(graph, graph.getNodeById("id1"), graph.getNodeById("id13"));
				} catch (NodeNotExistsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		};
		th = new Thread(run);
		th.start();
	}

	public static void update(INode node) throws InterruptedException {
		System.out.println("update"+node.getId());
		
	}

}

/**
 * 
 */
package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.io.XYGXLSupport;
import jpp.digraph.search.BFS;
import jpp.digraph.search.DFS;
import jpp.digraph.search.Dijkstra;
import jpp.digraph.search.IDiGraphSearchListener;
import jpp.digraph.search.XYAStar;

/**
 * @author genbob
 *
 */
public class TestSearch<N extends INode, E extends IEdge<INode>> {

	/**
	 * @param args
	 * @throws SAXException 
	 * @throws IOException 
	 * @throws ParserConfigurationException 
	 * @throws InvalidEdgeException 
	 * @throws NodeNotExistsException 
	 */
	public void test() throws InvalidEdgeException, ParserConfigurationException, IOException, SAXException, NodeNotExistsException {
		File file = new File("sample.gxl");
		FileInputStream fr = new FileInputStream(file);
		XYGXLSupport sup = new XYGXLSupport();
		DiGraph<XYNode, CostEdge<XYNode>> graph = sup.read(fr);
		System.out.println(graph.toString());
		BFS bfs = new BFS();
		DFS dfs = new DFS();
		XYAStar astar = new XYAStar<>();
		Dijkstra dij = new Dijkstra<>();
		INode[] source = new INode[graph.getNodeCount()];
		INode[] target = new INode[graph.getNodeCount()];
		graph.getNodes().toArray(source);
		graph.getNodes().toArray(target);
		for(INode node : graph.getNodes()){
		}
		dij.addListener(new IDiGraphSearchListener() {
			
			@Override
			public void onExpandNode(INode node, List way) {
				System.out.println(node.getId());
				
			}
			
		});
		List<CostEdge<INode>> list =  dij.search(graph, source[0], target[10]);
		

	}
	public static void main(String[] args) throws InvalidEdgeException, ParserConfigurationException, IOException, SAXException, NodeNotExistsException {
		TestSearch<INode, IEdge<INode>> test = new TestSearch<>();
		test.test();
		
	}


}

/**
 * 
 */
package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.exceptions.NodeNotExistsException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.ICostEdge;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.io.XYGXLSupport;
import jpp.digraph.search.BFS;
import jpp.digraph.search.DFS;
import jpp.digraph.search.Dijkstra;
import jpp.digraph.search.XYAStar;
import junit.framework.TestCase;
import junit.textui.TestRunner;

/**
 * @author genbob
 *
 */
public class TestSearch2 extends TestCase{

	
private DiGraph<INode, IEdge<INode>> graph;
INode test1;
INode test2;
INode test3;
INode test4;
INode test5;
INode test6;
INode test7;
IEdge<INode> edge1;
IEdge<INode> edge2;
IEdge<INode> edge3;
IEdge<INode> edge4;
IEdge<INode> edge5;
IEdge<INode> edge6;
	public TestSearch2() throws InvalidEdgeException, NodeNotExistsException, IllegalArgumentException{
		Collection<INode> nodes = new ArrayList<INode>();
		Collection<IEdge<INode>> edges = new ArrayList<IEdge<INode>>();
		test1 = new XYNode("test1", 1, 0);
		test2 = new XYNode("test2", 0, 1);
		test3 = new XYNode("test3", 1, 1);
		test4 = new XYNode("test4", 2, 1);
		 test5 = new XYNode("test5", 0, 2);
		 test6 = new XYNode("test6", 1, 2);
		 test7 = new XYNode("test7", 2, 2);
		 edge1 = new CostEdge<INode>("e1", test1, test2, 1);
		 edge2 = new CostEdge<INode>("e2", test1, test3, 1);
		edge3 = new CostEdge<INode>("e3", test1, test4, 1);
		 edge4 = new CostEdge<INode>("e4", test2, test5, 1);
		 edge5 = new CostEdge<INode>("e5", test3, test6, 1);
		 edge6 = new CostEdge<INode>("e6", test4, test7, 1);

		nodes.add(test1);
		nodes.add(test2);
		nodes.add(test3);
		nodes.add(test4);
		nodes.add(test5);
		nodes.add(test6);
		nodes.add(test7);

		edges.add(edge1);
		edges.add(edge2);
		edges.add(edge3);
		edges.add(edge4);
		edges.add(edge5);
		edges.add(edge6);
		this.graph = new DiGraph<INode, IEdge<INode>>(nodes, edges);
	}
	
//	public void testBFS() throws NodeNotExistsException, IllegalArgumentException{
//		BFS bfs = new BFS();
//		List<IEdge<INode>> list = bfs.search(this.graph,test1 , test6);
//		assertEquals(true, list.contains(edge2) & list.contains(edge5));
//		
//	}
//	public void testDFS() throws NodeNotExistsException{
//		DFS dfs = new DFS();
//		List<IEdge<INode>> list = dfs.search(this.graph, test1, test6);
////		assertEquals(true, list.contains(edge2) & list.contains(edge5));
//	}
//	public void testDijkstra() throws NodeNotExistsException{
//		Dijkstra dk = new Dijkstra();
//		List<IEdge<INode>> list = dk.search(this.graph, test1, test6);
//		assertEquals(true, list.contains(edge2) & list.contains(edge5));
//
//	}
//	public void testAStern() throws NodeNotExistsException{
//		XYAStar star = new XYAStar();
//		List<IEdge<INode>> list = star.search(this.graph, test1, test6);
//		assertEquals(true, list.contains(edge2) & list.contains(edge5));
//
//	}
	public void testSearchIO() throws InvalidEdgeException, ParserConfigurationException, IOException, SAXException, NodeNotExistsException{
		BFS bfs = new BFS();
		DFS dfs = new DFS();
		Dijkstra dk = new Dijkstra();
		XYAStar star = new XYAStar();

		
		XYGXLSupport sup = new XYGXLSupport();
		FileInputStream in = new FileInputStream(new File("sample.gxl"));
		DiGraph<XYNode, CostEdge<XYNode>> graph = sup.read(in);
		XYNode nodeSource = null;
		XYNode nodeTarget = null;
		Iterator<XYNode> iterator = graph.getNodes().iterator();
		while(iterator.hasNext()){
			XYNode node = iterator.next();
			if(node.getId().equals("id1")) nodeSource = node;
			if(node.getId().equals("id13")) nodeTarget = node;
		}
		
		bfs.addListener(new TestListener());
		dfs.addListener(new TestListener());
		dk.addListener(new TestListener());
		star.addListener(new TestListener());

		
//		List<CostEdge<INode>> listBFS = bfs.search(graph,nodeSource,	nodeTarget);
		List<CostEdge<INode>> listDFS = dfs.search(graph,nodeSource,	nodeTarget);
//		List<CostEdge<INode>> listdk = dk.search(graph,nodeSource,	nodeTarget);
//		List<CostEdge<INode>> listStar = star.search(graph,nodeSource,	nodeTarget);
//		for (CostEdge<INode> edge : listDFS){
//			System.out.println(edge.getSource().toString() + "\n");
//		}
	}
	
	
	
	
	public static void main(String[] args) {
		TestRunner.run(TestSearch2.class);

	}

}

/**
 * 
 */
package test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import jpp.digraph.exceptions.InvalidEdgeException;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.io.XYGXLSupport;

/**
 * @author genbob
 *
 */
public class TestIO {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws InvalidEdgeException 
	 * @throws TransformerException 
	 * @throws TransformerConfigurationException 
	 */
	public static void main(String[] args) throws IOException, InvalidEdgeException, ParserConfigurationException, SAXException, TransformerConfigurationException, TransformerException {
		XYGXLSupport sup = new XYGXLSupport();
		Collection<INode> nodes = new ArrayList<INode>();
		Collection<IEdge<INode>> edges = new ArrayList<IEdge<INode>>();
		INode test1 = new XYNode("test1", 0, 0);
		INode test2 = new XYNode("test2", 4, 1);
		INode test3 = new XYNode("test3", 3, -3);
		IEdge<INode> edge1 = new CostEdge<INode>("e1", test1, test2, 1.2);
		IEdge<INode> edge2 = new CostEdge<INode>("e2", test2, test1, 2.4);
		IEdge<INode> edge3 = new CostEdge<INode>("e3", test1, test3, 2.1);

		nodes.add(test1);
		nodes.add(test2);
		nodes.add(test3);
		edges.add(edge1);
		edges.add(edge2);
		edges.add(edge3);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DiGraph<XYNode, CostEdge<XYNode>> graph2 = new DiGraph(nodes, edges);
		graph2.setId("id1");
		sup.write(graph2, out);
		ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());
		
		byte[] bytes = new byte[bis.available()];
		bis.read(bytes, 0 , bis.available());
		String s = new String(bytes, StandardCharsets.ISO_8859_1);
		System.out.println(s);
		
//		DiGraph graph = sup.read(bis);
//		System.out.println(graph.toString());
		
	}
		
		
		
		

	

}

/**
 * 
 */
package test;

import java.util.List;

import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.stage.Stage;
import jpp.digraph.graph.CostEdge;
import jpp.digraph.graph.DiGraph;
import jpp.digraph.graph.IDiGraph;
import jpp.digraph.graph.IEdge;
import jpp.digraph.graph.INode;
import jpp.digraph.graph.XYNode;
import jpp.digraph.search.IDiGraphSearch;
import jpp.digraph.search.IDiGraphSearchListener;

/**
 * @author genbob
 *
 */
public class TaskTest extends Application{
	Task<Integer> task;
	private IDiGraphSearchListener<INode, IEdge<INode>> listener;
	private IDiGraphSearch search;
	private DiGraph<XYNode, CostEdge<XYNode>> graph;
	
	public TaskTest(IDiGraphSearch search, DiGraph<XYNode, CostEdge<XYNode>> graph){
		this.graph = graph;
		this.search = search;
		this.task = new Task<Integer>(){
			
			@Override
			protected Integer call() throws Exception{
				try{
					List<IEdge<INode>> list = search.search(graph, graph.getNodeById("id1"), graph.getNodeById("id13"));
					System.out.println(list.size());
				}catch (Exception e){
					
				}
				return 0;
			}
			
		};
	}
	
	
	public void update(int in){
		System.out.println(in);
	}
	
	


	@Override
	public void start(Stage arg0) throws Exception {
		Thread th = new Thread(this.task);
		th.setDaemon(true);
		th.start();
		
	}
	
	

}
